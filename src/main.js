const igdb = require('igdb-api-node').default
const fs = require('fs')

// Add your key from this file
const config = require('./config/config.json')

// Add your own IGDB API Key here
let API_KEY = config.API_KEY

async function gameSearch() {
	try {
		const response = await igdb(API_KEY)
			.fields(['age_ratings', 'category', 'cover', 'genres', 'name'])
			.limit(10)
			.query('category == 0;agre_ratings=1,11')
			.request('/games')

		//console.log(response.data)
		fs.writeFile('./results.json', JSON.stringify(response.data, null, 4), 'utf-8', (err) => {
			if (err) throw err
			console.log('Results have been written to disk!')
		})
	} catch(err) {
		console.error(err)
	}
}

gameSearch()
